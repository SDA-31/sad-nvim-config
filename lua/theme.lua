-- This file contains current colorscheme

-- My colorscheme there
local colorscheme = "dracula"

local status, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status then
	print("Colorscheme isn't exists.")
end

local module = require("tools.reqall")("transparent")
if not module then
	return
end

module.setup({
	enable = true, -- boolean: enable transparent
	extra_groups = { -- table/string: additional groups that should be cleared
		-- In particular, when you set it to 'all', that means all available groups

		-- example of akinsho/nvim-bufferline.lua
		-- "BufferLineTabClose",
		-- "BufferlineBufferSelected",
		"BufferLineFill",
		-- "BufferLineBackground",
		-- "BufferLineSeparator",
		-- "BufferLineIndicatorSelected",
	},
	exclude = {}, -- table: groups you don't want to clear
})

vim.cmd("hi NonText guifg=#495a8a")
