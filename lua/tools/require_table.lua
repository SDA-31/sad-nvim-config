-- param prefix (string): Prefix to current module
-- param table (table): Table of modules to require
-- param modulesWithSetup (table { string }): Modules that need to `setup(argsForSetup)` method call
-- param argsForSetup (table { ... }): Arguments for `setup(...)` method
return function(prefix, table, modules_with_setup, args_for_setup)
	for _, val in ipairs(table) do
		if modules_with_setup and require("tools.functions").contains(val, modules_with_setup) then
			require(prefix .. "." .. val).setup(args_for_setup)
		else
			require(prefix .. "." .. val)
		end
	end
end
