return function(module)
	local status, mod = pcall(require, module)
	if (status) then
		return mod
	else
		return false
	end
end
