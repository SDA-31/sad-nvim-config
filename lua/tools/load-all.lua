-- This code from https://github.com/mrjones2014/load-all.nvim

local function loadAll(path, fileExtension)
	local scan = require('plenary.scandir')
	for _, file in ipairs(scan.scan_dir(path, { depth = 0 })) do
		if file:sub(-#fileExtension) == fileExtension then
			dofile(file)
		end
	end
end

return loadAll
