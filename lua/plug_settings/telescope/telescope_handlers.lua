local this = {}

this.plugins_list = {
	"fzf",
	-- "media_files",
	"lsp_handlers",
}

this.opts = {
	defaults = {
		prompt_prefix = " ",
		selection_caret = " ",
		path_display = { "smart" },
	},
}

return this
