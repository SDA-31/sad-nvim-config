local this = {}

this.setup = function(prefix)
	local req = require "tools.require_table"
	local newprefix = prefix .. ".cmp"
	local requirements = {
		"cmp_conf",
		-- "cmp_plugins_conf"
	}

	req(newprefix, requirements)
end

return this

