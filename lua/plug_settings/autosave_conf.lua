vim.cmd("let g:auto_save = 1") -- enable AutoSave on Vim startup
vim.cmd("let g:auto_save_silent = 1") -- do not display the auto-save notification
