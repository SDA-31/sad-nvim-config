local this = {}

this.setup = function(prefix)
	local req = require("tools.require_table")

	local new_prefix = prefix .. ".lsp"

	local requirements = {
		"lsp_installer_conf",
		"lsp_conf",
		"null_ls_conf",
	}

	req(new_prefix, requirements, requirements, new_prefix)
end

return this
