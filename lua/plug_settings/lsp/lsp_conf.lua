-- This file contains LSP configurations

local this = {}

this.setup = function(prefix)
	local lsp_handlers = require(prefix .. ".lsp_handlers")

	local lspconf = require("tools.reqall")("lspconfig")
	if not lspconf then
		return
	end

	local lsp_opts = {
		on_attach = lsp_handlers.on_attach,
		capabilities = lsp_handlers.capabilities,
		offset_encoding = lsp_handlers.offset_encoding,
	}

	for _, i in pairs(lsp_handlers.lsp_list) do
		local is_valid, server_opts = pcall(require, prefix .. ".settings." .. i)
		local lsp_path = nil

		if is_valid then
			if require("tools.functions").contains(i, lsp_handlers.path_needed_for) then
				lsp_path = lsp_handlers.lsp_path
			end

			server_opts = vim.tbl_deep_extend("force", lsp_opts, server_opts(lsp_path))

			lspconf[i].setup(server_opts)
		else
			lspconf[i].setup(lsp_opts)
		end
	end
end

return this
