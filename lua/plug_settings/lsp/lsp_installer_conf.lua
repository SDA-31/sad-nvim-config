-- LSP Installer preferences

local this = {}

this.setup = function(prefix)
	local lsp_handlers = require(prefix .. ".lsp_handlers")

	local lsp_installer = require("tools.reqall")("nvim-lsp-installer")
	if not lsp_installer then
		return
	end

	lsp_installer.setup({
		ensure_installed = lsp_handlers.lsp_list, -- ensure these servers are always installed
		automatic_installation = true, -- automatically detect which servers to install (based on which servers are set up via lspconfig)
		ui = {
			icons = {
				server_installed = "✓",
				server_pending = "➜",
				server_uninstalled = "✗",
			},
		},
		install_root_dir = lsp_handlers.lsp_path,
	})
end

return this
