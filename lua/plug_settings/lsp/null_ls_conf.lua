local this = {}

this.setup = function(prefix)
	local null_ls = require("tools.reqall")("null-ls")
	if not null_ls then
		return
	end

	local lsp_handlers = require(prefix .. ".lsp_handlers")

	local formatting = null_ls.builtins.formatting
	local diagnostics = null_ls.builtins.diagnostics
	-- local code_actions = null_ls.builtins.code_actions

	local config_path = lsp_handlers.editor_configs_path

	null_ls.setup({
		on_init = function(new_client, _)
			new_client.offset_encoding = lsp_handlers.offset_encoding
		end,

		sources = {
			formatting.prettier.with({ extra_args = { "--config", config_path .. ".prettierrc" } }),
			formatting.stylua.with({ extra_args = { "--config-path", config_path .. ".stylua.toml" } }),
			formatting.autopep8.with({ extra_args = { "--aggressive", "--aggressive" } }),
			formatting.rustfmt.with({ extra_args = { "--config-path", config_path .. ".rustfmt.toml" } }),
			-- Available in clang 14.0
			-- formatting.clang_format.with({ extra_args = { "-style=file:" .. config_path .. ".clang-format" } }),

			diagnostics.flake8.with({ extra_args = { "--config", config_path .. ".flake8" } }),
			diagnostics.pylint.with({ extra_args = { "--rcfile=" .. config_path .. ".pylintrc" } }),
			diagnostics.luacheck.with({ extra_args = { "--globals", "vim" } }),
		},
		-- debug = true,
	})
end

return this
