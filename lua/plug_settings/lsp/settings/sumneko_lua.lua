return function(--[[ lsp_path ]])
	-- local path = require "plenary.path"
	--
	-- local sumneko_root_path = path.new(lsp_path, "sumneko_lua", "extension", "server")
	-- local sumneko_bin_path = path.joinpath(sumneko_root_path, "bin")

	return {
		-- cmd = { path.joinpath(sumneko_bin_path, "lua-language-server").filename, "-E",
		-- 	path.joinpath(sumneko_root_path, "main.lua").filename },
		settings = {
			Lua = {
				runtime = {
					-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
					version = "LuaJIT",
				},
				diagnostics = {
					-- Get the language server to recognize the `vim` global
					globals = { "vim" },
				},
				workspace = {
					-- Make the server aware of Neovim runtime files
					library = vim.api.nvim_get_runtime_file("", true),
				},
				-- Do not send telemetry data containing a randomized but unique identifier
				telemetry = {
					enable = false,
				},
				format = {
					enable = true,

					defaultConfig = {
						max_line_length = "100",
					},
				},
			},
		},
	}
end
