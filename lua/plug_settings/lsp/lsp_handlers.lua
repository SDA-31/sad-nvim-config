-- This file contains different handlers for lsp configuration

local this = {}

local path = require("tools.reqall")("plenary.path")
if not path then
	return
end

-- Servers list
this.lsp_list = {
	"bashls",
	"clangd",
	"cmake",
	"dockerls",
	"jsonls",
	-- "kotlin_language_server",
	"lemminx",
	"omnisharp",
	"pyright",
	"remark_ls",
	"rust_analyzer",
	"sqlls",
	"sumneko_lua",
	"vimls",
	"yamlls",
}

this.editor_configs_path = require("plenary.path").new(vim.fn.stdpath("config"), "editor_configs").filename
	.. require("tools.functions").sep()

-- List of formatters used instead LSP servers-provided
this.formatters_list = {
	["jsonls"] = "prettier",
	["pyright"] = "black",
	["sumneko_lua"] = "stylua",
	["clangd"] = "clang_format",
	["remark_ls"] = "prettier",
	["rust_analyzer"] = "rustfmt",
}

this.setup = function()
	local signs = {
		{ name = "DiagnosticSignError", text = "" },
		{ name = "DiagnosticSignWarn", text = "" },
		{ name = "DiagnosticSignHint", text = "" },
		{ name = "DiagnosticSignInfo", text = "" },
	}
	for _, sign in ipairs(signs) do
		vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
	end

	vim.diagnostic.config({
		-- disable virtual text
		virtual_text = false,
		-- show signs
		signs = {
			active = signs,
		},
		update_in_insert = true,
		underline = true,
		severity_sort = true,
		float = {
			focusable = false,
			style = "minimal",
			border = "rounded",
			source = "always",
			header = "",
			prefix = "",
		},
	})
	vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
		border = "rounded",
	})

	vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
		border = "rounded",
	})
end

-- Path of installation dir for LSP servers
this.lsp_path = path.new(vim.fn.stdpath("data"), "lsp_servers").filename

-- Sets the encoding for LSP
this.offset_encoding = "utf-8"

-- Useless
this.path_needed_for = {
	-- "sumneko_lua",
}

local function lsp_highlight_document(client)
	-- Set autocommands conditional on server_capabilities
	if client.resolved_capabilities.document_highlight then
		vim.api.nvim_exec(
			[[
			augroup lsp_document_highlight
				autocmd! * <buffer>
				autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
				autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
			augroup END ]],
			false
		)
	end
end

-- Keymaps
local function lsp_keymaps(bufnr)
	local opts = { noremap = true, silent = true }
	local bufopts = { noremap = true, silent = true, buffer = bufnr }

	-- Shortcuts for LSP
	-- Shortcuts without bufnr needed
	vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, opts)
	vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
	vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts)
	vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist, opts)

	-- Shortcuts with bufnr needed
	vim.keymap.set("n", "gD", vim.lsp.buf.declaration, bufopts)
	vim.keymap.set("n", "gd", vim.lsp.buf.definition, bufopts)
	vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
	vim.keymap.set("n", "<leader>gi", vim.lsp.buf.implementation, bufopts)
	vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, bufopts)
	-- vim.keymap.set("n", "<leader>wa", vim.lsp.buf.add_workspace_folder, bufopts)
	-- vim.keymap.set("n", "<leader>wr", vim.lsp.buf.remove_workspace_folder, bufopts)
	-- vim.keymap.set("n", "<leader>wl", function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end, bufopts)
	vim.keymap.set("n", "<leader>D", vim.lsp.buf.type_definition, bufopts)
	vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, bufopts)
	vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, bufopts)
	vim.keymap.set("n", "gr", vim.lsp.buf.references, bufopts)
	vim.keymap.set("n", "<leader>f", vim.lsp.buf.formatting, bufopts)
	vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist, bufopts)

	-- Alias for formatting command
	vim.cmd([[ command! Format execute "lua vim.lsp.buf.formatting()" ]])
end

this.on_attach = function(client, bufnr)
	if require("tools.functions").contains_key(client.name, this.formatters_list) then
		client.resolved_capabilities.document_formatting = false
	end

	lsp_keymaps(bufnr)
	lsp_highlight_document(client)
end

this.capabilities = require("cmp_nvim_lsp").update_capabilities(vim.lsp.protocol.make_client_capabilities())

return this
