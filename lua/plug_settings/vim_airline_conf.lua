local cmd = vim.cmd

-- Autoreloading
cmd([[
	augroup airline_auto_config
		autocmd!
		autocmd BufWritePost vim_airline.lua source <afile> | AirlineRefresh
	augroup end
]])

-- Airline SmartTab
-- vim.g.airline#extensions#tabline#buffer_idx_mode = 1

-- Visuals
-- Separators
vim.g.airline_left_sep = ""
vim.g.airline_right_sep = ""

-- vim.g.airline#extensions#tabline#left_sep = ""
-- vim.g.airline#extensions#tabline#left_alt_sep = "|"

-- After changing this code, restart nvim (:AirlineRefresh doesn't work for this)
-- Tabs
-- SmartTab
-- vim.g.airline#extensions#tabline#enabled = 1
-- vim.g.airline#extensions#tabline#show_splits = 1

-- Path type
-- vim.g.airline#extensions#tabline#formatter = "unique_tail_improved"

-- vim.g.airline#extensions#tabline#show_tab_type = 0
