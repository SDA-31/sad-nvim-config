local opts = { noremap = true, silent = true }
-- local term_opts = { noremap = true }

local keymap = vim.api.nvim_set_keymap

keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Command shortcuts

-- Keyboard shortcuts --
-- Copy keymaps
keymap("v", "<C-A-y>", [["+y]], opts)
keymap("x", "<C-A-y>", [["+y]], opts)
keymap("n", "<C-A-p>", [["+p]], opts)
keymap("i", "<C-A-p>", [[<ESC>"+pa]], opts)
keymap("v", "<C-A-p>", [["+p]], opts)
keymap("x", "<C-A-p>", [["+p]], opts)

-- Code manipulating keymaps
-- Moving lines
keymap("n", "<A-j>", ":MoveLine(1)<CR>", opts)
keymap("n", "<A-k>", ":MoveLine(-1)<CR>", opts)
keymap("v", "<A-j>", ":MoveBlock(1)<CR>", opts)
keymap("v", "<A-k>", ":MoveBlock(-1)<CR>", opts)
keymap("v", "<A-S-l>", ":MoveHBlock(1)<CR>", opts)
keymap("v", "<A-S-h>", ":MoveHBlock(-1)<CR>", opts)

-- Increace tabulation
keymap("n", "<A-l>", [[>>]], opts)
keymap("v", "<A-l>", [[>gv]], opts)
keymap("n", "<A-h>", [[<<]], opts)
keymap("v", "<A-h>", [[<gv]], opts)

-- Plugins keymaps --
-- Code formatter

-- Other
-- keymap("", "+", "<Plug>(expand_region_expand)")
-- keymap("", "_", "<Plug>(expand_region_shrink)")

-- keymap("i", "jk", "<ESC>", opts)
keymap("n", "<F8>", ":TagbarToggle<CR>", opts)

-- Open/Reset NTree
keymap("n", "<C-A-n>", ":NERDTree<CR>", opts)
keymap("v", "<C-A-n>", ":NERDTree<CR>", opts)

-- Toggle NTree
keymap("n", "<C-t>", ":NERDTreeToggle<CR>", opts)
keymap("v", "<C-t>", ":NERDTreeToggle<CR>", opts)

-- Comment the line
keymap("n", "<C-A-k>", ":Commentary<CR>", opts)
keymap("v", "<C-A-k>", ":Commentary<CR>", opts)

-- Bufferline etc
-- Pick tab
keymap("n", "<leader>t", ":BufferLineCycleNext<CR>", opts)
keymap("n", "<leader>T", ":BufferLineCyclePrev<CR>", opts)
keymap("n", "<leader>p", ":BufferLinePick<CR>", opts)

for i = 0, 9 do
	keymap("n", "<leader>" .. i, ":BufferLineGoToBuffer" .. i .. "<CR>", opts)
end

-- Close buffers
keymap("n", "<leader>'", ":BufferLinePickClose<CR>", opts)
keymap("n", '<leader>"', ":bd!<CR>", opts)
-- keymap("n", "<leader><C-'>", ":BufferLineCloseLeft<CR>", opts)
-- keymap("n", "<leader><C-\">", ":BufferLineCloseRight<CR>", opts)
keymap("n", "<leader><A-'>", ":BDelete all<CR>", opts)

-- Telescope mappings
local telescope = require("tools.reqall")("telescope")
if telescope then
	-- keymap("n", "<leader>sf", "<cmd>Telescope find_files<cr>", opts)
	-- TODO: change to vim.keymap.set function
	keymap(
		"n",
		"<leader>sf",
		"<cmd>lua require'telescope.builtin'.find_files(require'telescope.themes'.get_dropdown())<cr>",
		opts
	)
	keymap("n", "<leader>sg", "<cmd>Telescope live_grep<cr>", opts)
end
