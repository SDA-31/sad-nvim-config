# SAD Neovim Config

Этот конфиг предназначен для девелопмента (C++, Python, Rust, etc.).
Этот README файл, я оставляю как памятку/заполнитель пустого пространства в моём гитлабе.

## Фичи

На данный момент, в этом конфиге реализовано:

- Поддержка тем, а также поддержка прозрачного фона

- Подсветка синтаксиса

- Автосохранение

- Управление вкладками. Тут, стоит отметить то, что вкладок, в привычном понимании, в этом конфиге нет -
  всё базируется на [_буфферах_](https://neovim.io/doc/user/windows.html#buffers/). Вкладки отображаются в
  правом верхнем углу. **Также, стоит отметить, что при закрытии буфера,
  все несрхранённые изменения откатываются**

- Автодополнение кода (на данный момент из буффера)

- Превью _.md_ файлов в интерактивном режиме, с помощью браузера ([markdown-preview.nvim](https://github.com/iamcco/markdown-preview.nvim/))

- Работа с Git:

  - Консольная работа с Git ([vim-fugitive](https://github.com/tpope/vim-fugitive/))

  - Сравнение коммитов ([diffview.nvim](https://github.com/sindrets/diffview.nvim/))

  - Полезный визуальный плагин для отображения изменённых строк ([gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim/))

  - Плагин для отображения статуса файлов в NerdTree ([nerdtree-git-plugin](https://github.com/Xuyuanp/nerdtree-git-plugin/))

  - Клон [Magit](https://magit.vc/) для Neovim ([neogit](https://github.com/TimUntersberger/neogithttps://github.com/TimUntersberger/neogit/))

- Куча кастомных (моих) комбинаций горячих клавиш

- Комментирование строк и блоков текста

- Перемещение строк и блоков кода

- Поддержка LSP

- И др.

## Планируется

- Полная поддержка [nvim-treesiter](https://github.com/nvim-treesitter/nvim-treesitter/)
- Полная поддержка [Telescope](https://github.com/nvim-telescope/telescope.nvim/)
- Автозакрытие скобок и кавычек
- Поддержка отладки кода
- Добавить страничку Wiki

## Зависимости

- Neovim >= 0.7.0
- Git
- Curl & Wget
- GCC
- Cmake
- unzip, tar, gzip
- Node.js (LTS) & npm
- Python3 & pip3
- [Rust](https://www.rust-lang.org/learn/get-started/)
- [fzf](https://github.com/junegunn/fzf/)
- [ripgrep](https://github.com/BurntSushi/ripgrep/)
- [fd](https://github.com/sharkdp/fd/)
- [Überzug](https://github.com/seebye/ueberzug/)
- Шрифты:
  - [Powerline Fonts](https://github.com/powerline/fonts/)
  - [Hack](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Hack/)

### LSP

Здесь приведён список дополнительных зависимостей для LSP (**обязательных в данном конфиге**).
Перед настройкой зависимостей, настоятельно советую посетить репозиторий [null-ls](https://github.com/jose-elias-alvarez/null-ls.nvim/)
Стоит обратить внимание, что можно поставить сколько угодно форматтеров, линтеров и т.п.

- LSP экосистемы:
  - [remark](https://github.com/remarkjs/remark/) - экосистема для Markdown
- Форматтеры:
  - [Prettier](https://prettier.io/docs/en/install/) - маст хэв для любого веб-девелопера. Советую посетить домашнюю страницу программы.
  - Python:
    - [autopep8](https://pypi.org/project/autopep8/)
    - [Black](https://pypi.org/project/black/)
    - [YAPF](https://github.com/google/yapf/)
- Линтеры:
  - Python:
    - [Flake8](https://pypi.org/project/flake8/)
    - [PyLint](https://pypi.org/project/pylint/)
  - Lua:
    - [Luacheck](https://github.com/mpeterv/luacheck/)

Пакеты для **полной** установки серверов из [nvim-lsp-installer](https://github.com/williamboman/nvim-lsp-installer/)
(**_советую устанавливать по мере необходимости_**):

- go >= 1.17
- JDK
- Ruby & gem
- dotnet
- pwsh (_лол, винда_)
- Julia
- valac (and menson & ninja)
- rebar3
- cargo

### Telescope

Здесь приведён список дополнительных зависимостей для Telescope.

- [ffmpegthumbnailer](https://github.com/dirkvdb/ffmpegthumbnailer/) - превью видео
- [pdftoppm](https://poppler.freedesktop.org/) - так, на всякий, если этого нет из коробки (_серьёзно, кто-то будет юзать мой конфиг? да и тем болеее с LFS/Gentoo/Slackware?_)
- [epub-thumbnailer](https://github.com/marianosimone/epub-thumbnailer/) - превью EPub
- [fontpreview](https://github.com/sdushantha/fontpreview/) - превью шрифтов

## Установка

**ВНИМАНИЕ**, это удалит старый конфиг! Настоятельно рекомендую сделать бэкап или подумать своей головой (_а оно вам надо?_)

```bash
mkdir ~/.config 2>/dev/null
rm -rf ~/.config/nvim
git clone https://gitlab.com/SDA-31/sad-nvim-config
mv sad-nvim-config/ ~/.config/nvim
nvim
```

В случае ошибки установки плагинов, нажать `R` на клавиатуре столько раз, сколько попыток потребуется, для полной их загрузки.
Это может произойти в случае превышения количества запросов к Github API, или слабого интернет-соединения.
В случае случайного закрытия окна пакера, ввести `:PackerSync`.

После установки, перезайти в Neovim (2 раза, поскольку после перезахода, начнётся установка LSP-серверов).
